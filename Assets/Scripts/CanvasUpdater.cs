﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasUpdater : MonoBehaviour
{
	// Use this for initialization
	void Start ()
    {
        Player player = GameObject.Find("Player").GetComponent<Player>();
        for (int i = 0; i < player.weapons.Count; i++)
        {
            player.weapons[i].cooldownbar = this.transform.GetChild(i).GetComponent<SpecialAbilityBar>();
            this.transform.GetChild(i).gameObject.SetActive(true);
        }
	}
}
