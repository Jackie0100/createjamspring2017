﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatedPlatform : MonoBehaviour
{
    public IEnumerator RaisePlatform()
    {
        this.GetComponent<SpriteRenderer>().color = Color.white;

        while (this.transform.position.y < 1)
        {
            this.transform.position += (Vector3.up * Time.deltaTime);
            yield return null;
        }
    }
}
