﻿using UnityEngine;

public class Weapon
{
    public Tape currentTape;
    [SerializeField]
    LayerMask thingstohit = 0x11111111;

    public void TriggerSpecialAbility(Player player)
    {
        RaycastHit2D hit;
        switch (currentTape.specialability)
        {
            case SpecialAbility.EnemyStun:
                break;
            case SpecialAbility.JumpBoost:
                player.JumpBoost();
                break;
            case SpecialAbility.SpeedBoost:
                break;
            case SpecialAbility.AnimatePlatforms:
                hit = Physics2D.Raycast(player.transform.position, player.GetAimDirection(), 10, 1);
                if (hit != null && hit.transform != null && hit.transform.GetComponent<AnimatedPlatform>() != null)
                {
                    player.StartCoroutine(hit.transform.GetComponent<AnimatedPlatform>().RaisePlatform());
                }
                break;
            case SpecialAbility.FreezeBeam:
                hit = Physics2D.Raycast(player.transform.position, player.GetAimDirection(), 10, 1);
                Debug.Log("Trying to freeze object: " + hit.transform.name);
                if (hit != null && hit.transform.GetComponent<WaterDetector>() != null)
                {
                    var index = hit.transform.GetComponent<WaterDetector>().Index;
                    Water.instance.FreezeWater(index, 10);
                }
                break;
        }
    }

    public void Shoot(Player player)
    {
        currentTape.Shoot(player);
    }
}