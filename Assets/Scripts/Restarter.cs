using System;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Restarter : MonoBehaviour
{
    Vector2 startpos;

    private void Start()
    {
        startpos = Player.instance.transform.position;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            other.transform.position = startpos;
        }
    }
}
