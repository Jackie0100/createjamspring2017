﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletWave : MonoBehaviour {

    public Vector3 dir;
    public float speed = 10;

    [SerializeField]
    private AudioClip _audioClip;

    [SerializeField] private string _message;
    [SerializeField] LayerMask thingstohit;

    // Use this for initialization
	void Start ()
    {
        this.GetComponent<SpriteRenderer>().material.mainTextureScale = new Vector2(0, 1);
        this.transform.localScale = new Vector3(0, 0.1f, 0);
	    AudioSource.PlayClipAtPoint(_audioClip, transform.position);
	    Player.instance.GetComponent<OverlayMusicHandler>().Ping();
	    Destroy(gameObject, 5);
	} 

	// Update is called once per frame
	void Update ()
	{
        transform.Translate(dir * Time.deltaTime * speed);
        this.GetComponent<SpriteRenderer>().material.mainTextureOffset = new Vector2(this.GetComponent<SpriteRenderer>().material.mainTextureOffset.x + (Time.deltaTime * speed), 0);

        var hit = Physics2D.Raycast(transform.position, transform.forward, this.transform.lossyScale.x, thingstohit);
        if (hit)
        {
            if (hit && hit.transform && hit.transform.GetComponent<Enemy>() != null)
            {
                Destroy(hit.transform.gameObject);
            }
            hit.transform.SendMessage(_message, SendMessageOptions.DontRequireReceiver);
            Destroy(gameObject);
        }

        if (this.GetComponent<SpriteRenderer>().material.mainTextureScale.x <= 3)
        {
            // Debug.Log(this.GetComponent<SpriteRenderer>().material.mainTextureScale.x);

            this.GetComponent<SpriteRenderer>().material.mainTextureScale += new Vector2(Time.deltaTime * 5 * speed, 0);
            this.transform.localScale += new Vector3((Time.deltaTime * 5 * speed) / 6.0f, 0, 0);
        }
	}
}
