﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TapeItem : ItemPickUp
{
    private float _startheight;
    [SerializeField]
    private float _pingpongheight = 1;
    [SerializeField]
    private float _speed;
    [SerializeField]
    private Tape _tape;

    [SerializeField] private AudioClip _pickUpSound;

    // Use this for initialization
    protected override void Start ()
    {
        _startheight = transform.position.y;
    }

    // Update is called once per frame
    protected override void Update ()
    {
        transform.position = new Vector3(transform.position.x, _startheight + Mathf.Sin(Time.time * _speed * Mathf.Rad2Deg) * _pingpongheight, transform.position.z);
    }

    protected override void PickUp(Player player)
    {
        GetComponent<SpriteRenderer>().enabled = false;
        GetComponent<BoxCollider2D>().enabled = false;
        AudioSource.PlayClipAtPoint(_pickUpSound, transform.position);
        player.weapons.Add(_tape);
        _tape.SetActiveTape(player);
        base.PickUp(player);
    }
}
