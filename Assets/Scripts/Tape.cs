﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Tape
{
    public GameObject ammo;
    public AudioClip musicIntro;
    public AudioClip music;
    public AudioClip overlay;
    public AudioClip sfx;
    public SoundWaveType wavetype;
    public SpecialAbility specialability;
    public int specialAbilityCharges;
    public int specialAbilityMaxCharges;
    public float specialAbilityCoolDown;
    public float basedamage;
    public float firerate;
    [SerializeField]
    private float _fadeDuration = 1f;
    [SerializeField]
    public SpecialAbilityBar cooldownbar;


    private float _cooldownCounter;

    private float _fireCoolDown;

    public void UpdateTapeCoolDown()
    {
        _fireCoolDown += Time.deltaTime;
        if (specialAbilityCharges != specialAbilityMaxCharges)
        {
            _cooldownCounter += Time.deltaTime;

            if (_cooldownCounter >= specialAbilityCoolDown)
            {
                specialAbilityCharges++;
                _cooldownCounter = 0;
            }
        }
        cooldownbar.UpdateBar(GetChargeCoolDownPercentage());
    }

    public void Shoot(Player player)
    {
        if (_fireCoolDown >= firerate)
        {
            _fireCoolDown = 0;
            GameObject go = GameObject.Instantiate(ammo);

            go.transform.position = (Vector2)player.transform.GetChild(0).GetChild(0).transform.position;
            go.transform.rotation = Quaternion.Euler(0, 0, Mathf.Atan2(player.GetAimDirection().y, player.GetAimDirection().x) * Mathf.Rad2Deg);
            go.GetComponent<BulletWave>().dir = -Vector2.left;
        }
    }

    public float GetChargeCoolDownPercentage()
    {
        if (specialAbilityCharges == specialAbilityMaxCharges)
        {
            return 1;
        }
        else
        {
            return _cooldownCounter / specialAbilityCoolDown;
        }
    }

    public Tape SetActiveTape(Player player)
    {
        cooldownbar.gameObject.SetActive(true);
        player.weapon.currentTape = this;
        player.StartCoroutine(SwitchToMusic());
        return this;
    }

    private IEnumerator SwitchToMusic()
    {
        var musicSource = Player.instance.musicSource;
        var startVolume = musicSource.volume;
        var startTime = Time.time;
        while (musicSource.volume > 0.01f)
        {
            musicSource.volume = Mathf.Lerp(startVolume, 0, (Time.time - startTime) / _fadeDuration);
            yield return null;
        }
        musicSource.Stop();
        musicSource.clip = musicIntro;
        musicSource.volume = startVolume;
        musicSource.loop = false;
        musicSource.Play();

        while (musicSource.isPlaying)
            yield return null;

        musicSource.clip = this.music;
        musicSource.loop = true;
        musicSource.Play();

        var overlaySource = Player.instance.overlaySource;
        overlaySource.clip = overlay;
        overlaySource.loop = true;

        musicSource.Play();
        overlaySource.Play();
    }
}

public enum SoundWaveType
{
    Sine,
    Square,
    Saw,
    Triangle,
}

public enum SpecialAbility
{
    JumpBoost,
    EnemyStun,
    SpeedBoost,
    FreezeBeam,
    AnimatePlatforms,
}