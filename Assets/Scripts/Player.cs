﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Player : MonoBehaviour
{
    public AudioSource musicSource;
    public AudioSource sfxSource;



    [SerializeField]
    Animator playerAnimation;

    [SerializeField]
    bool _useJoystick;
    [SerializeField]
    Transform _targetWeaponArm;
    [SerializeField]
    float _moveSpeed = 1;
    [SerializeField]
    float _cameraLookAhead = 2;
    [SerializeField]
    float _dampen = 1;
    Vector3 m_CurrentVelocity;

    bool _grounded = false;
    [SerializeField]
    float _jumpForce = 5;

    float jumpTime;
    float jumpAcceleration;
    float counter;
    float velocityMax;

    bool isJumping;
    bool canJump;

    public List<Tape> weapons;

    public Weapon weapon;

    public Vector2 movedir;

    public static Player instance;

    public OverlayMusicHandler OverlayHandler;
    public AudioSource overlaySource;

    [SerializeField] private AudioClip _startAudioClip;

    // Use this for initialization
    void Awake ()
    {
        DontDestroyOnLoad(this);
        if (instance == null)
        {
            instance = this;
        }

        weapons = new List<Tape>();
        weapon = new Weapon();

        musicSource = this.gameObject.AddComponent<AudioSource>();
        musicSource.clip = _startAudioClip;
        musicSource.loop = true;
        musicSource.volume = 0.3f;
        musicSource.Play();
        sfxSource = this.gameObject.AddComponent<AudioSource>();
        overlaySource = this.gameObject.AddComponent<AudioSource>();
        overlaySource.loop = true;
        overlaySource.volume = 0.3f;
        OverlayHandler = this.gameObject.AddComponent<OverlayMusicHandler>();
        OverlayHandler._audioSource = overlaySource;
    }

    // Update is called once per frame
    void Update ()
    {
        foreach (Tape t in weapons)
        {
            t.UpdateTapeCoolDown();
        }

        if (Input.GetKeyDown(KeyCode.Alpha1) && weapons.Count >= 1)
        {
            weapons[0].SetActiveTape(this);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2) && weapons.Count >= 2)
        {
            weapons[1].SetActiveTape(this);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3) && weapons.Count >= 3)
        {
            weapons[2].SetActiveTape(this);
        }

        this.GetComponent<BoxCollider2D>().bounds.SetMinMax(this.GetComponent<SpriteRenderer>().sprite.bounds.min, this.GetComponent<SpriteRenderer>().sprite.bounds.max);

        movedir = new Vector2(Input.GetAxis("Horizontal"), 0/*Input.GetAxis("Vertical")*/);
        playerAnimation.SetBool("IsRunning", movedir.x != 0);
        this.transform.Translate(movedir * Time.deltaTime * _moveSpeed);


        if (movedir.x > 0.001f)
        {
            this.transform.localScale = new Vector3(-0.5f, 0.5f, 0.5f);
        }
        else if (movedir.x < -0.001f)
        {
            this.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
        }

        //this.GetComponent<SpriteRenderer>().flipX = (movedir.x < 0.1f);
        //Jump Logic
        /*if (_grounded && (Input.GetKey(KeyCode.Space) || Input.GetKey(KeyCode.Joystick1Button0)))
        {
            // Add a vertical force to the player.
            _grounded = false;
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, _jumpForce));

        }*/

        if (_grounded && Input.GetButtonDown("Jump"))
        {
            playerAnimation.SetBool("IsJumping", true);

            canJump = false;
            isJumping = true;
            _grounded = false;
            this.GetComponent<Rigidbody2D>().velocity =new Vector2(0f, 5);

        }

        if (Input.GetButtonUp("Jump"))
        {
            counter = 0;
            isJumping = false;
            canJump = _grounded;
        }

        //Camera stuff

        Camera.main.transform.position = new Vector3(this.transform.localPosition.x, this.transform.localPosition.y, -10);// Vector3.SmoothDamp(new Vector3(this.transform.position.x + movedir.x * _cameraLookAhead, this.transform.position.y /*+ movedir.y * _cameraLookAhead*/, -10), this.transform.position, ref m_CurrentVelocity, _dampen);

        //Gun/Arm
        CalculateGunArmRotation();

        //Shoot/Weapon
        if (Input.GetKeyDown(KeyCode.Mouse1) || Input.GetKeyDown(KeyCode.JoystickButton4))
        {
            if (weapon.currentTape != null && weapon.currentTape.specialAbilityCharges > 0)
            {
                weapon.TriggerSpecialAbility(this);
                weapon.currentTape.specialAbilityCharges--;
            }
        }
        if (Input.GetKey(KeyCode.Mouse0) || Input.GetKey(KeyCode.JoystickButton5))
        {
            if (weapon.currentTape != null)
            {
                weapon.Shoot(this);
            }
        }
    }

    public Vector2 GetAimDirection()
    {
        return (Camera.main.ScreenToWorldPoint(Input.mousePosition) - this.transform.position);
    }

    private void FixedUpdate()
    {
        if (!isJumping || !(counter < 0.5f)) return;
        counter += Time.deltaTime;
        GetComponent<Rigidbody2D>().velocity += new Vector2(0f, 20 * Mathf.Abs(counter - 0.5f) * Time.deltaTime);
    }

    public void JumpBoost()
    {
        GetComponent<Rigidbody2D>().AddForce(-(GetAimDirection().normalized) * 5, ForceMode2D.Impulse);
        foreach (var rig in Physics2D.OverlapCircleAll(transform.position + transform.forward, 5).Where(c => c.GetComponent<Rigidbody2D>()!=null).Select(c => c.GetComponent<Rigidbody2D>()))
        {
            var dir = rig.transform.position - transform.position;
            rig.AddForce(dir.normalized*(5 - dir.magnitude), ForceMode2D.Impulse);
        }
    }

    void CalculateGunArmRotation()
    {
        Vector2 playerpos = new Vector2(this.transform.position.x, this.transform.position.y);
        Vector2 mousepos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        /*if (_useJoystick)
        {
            mousepos = new Vector2(Input.GetAxis("AimHorizontal"), -Input.GetAxis("AimVertical"));
            Debug.Log(mousepos);

            if (Mathf.Approximately(mousepos.x, 0) && Mathf.Approximately(mousepos.y, 0))
            {
                if (this.GetComponent<SpriteRenderer>().flipX)
                {
                    mousepos = Vector2.left;
                    _targetWeaponArm.position = playerpos + mousepos;

                }
                else
                {
                    mousepos = Vector2.left;
                    _targetWeaponArm.position = playerpos - mousepos;
                }
            }
            else
            {
                mousepos.Normalize();
                _targetWeaponArm.position = playerpos + mousepos;
            }
        }
        else*/
        {
            //go.transform.position = (Vector2)this.transform.position + GetAimDirection();
            Vector2 aimdir = GetAimDirection();
            _targetWeaponArm.transform.rotation = Quaternion.Euler(0, 0, (Mathf.Atan2(aimdir.y, aimdir.x) * Mathf.Rad2Deg) + ((this.transform.localScale.x >= 0) ? 180 : 0));
            //_targetWeaponArm.position = playerpos + new Vector2(mousepos.x - playerpos.x, mousepos.y - playerpos.y).normalized;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        _grounded = true;
        playerAnimation.SetBool("IsJumping", false);
    }
}
