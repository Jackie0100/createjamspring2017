﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class InventoryBehaviour : MonoBehaviour
{
    [SerializeField] private RectTransform _inventoryTransform;
    [SerializeField] private float _animationDuration = 3f;
    [SerializeField] private int _openPosition = 115;
    [SerializeField] private int _closedPosition = 15;
    [SerializeField] private Transform _cassetteListRoot;
    [SerializeField] private GameObject _cassetteObject;

    private float _animationDelta;  // Pixels pr second

    private readonly HashSet<Tape> _tapes = new HashSet<Tape>();

    public AnimationState State { get; private set; }

    public enum AnimationState
    {
        Closed,
        Opening,
        Open,
        Closeing
    }

    void Start()
    {
        if (_openPosition < _closedPosition)
        {
            Debug.LogError("Open position must be greater than or equal to the closed position.");
            enabled = false;
        }

        if (_inventoryTransform == null)
        {
            Debug.LogError("Inventory Transform must be set to something.");
            enabled = false;
        }

        State = AnimationState.Closed;
        _animationDelta = (_openPosition - _closedPosition) / _animationDuration;
    }

    void Update()
    {
        UpdateControllerInput();
        UpdateAnimation();
    }

    private void UpdateControllerInput()
    {
        if (Input.GetButtonDown("Fire3"))
        {
            ToggleInventory();
        }
        if (State != AnimationState.Open) return;

        if (Input.GetButtonDown("D Pad Horizontal"))
        {
            if (Input.GetAxis("D Pad Horizontal") > 0)
            {
                _selectedIndex++;
                _selectedIndex = _selectedIndex >= _tapes.Count ? _tapes.Count - 1 : _selectedIndex;
            }
            else
            {
                _selectedIndex--;
                _selectedIndex = _selectedIndex < 0 ? 0 : _selectedIndex;
            }
            UpdateSelectedItem();
        }

        // TODO: When an item is selected
    }

    private int _selectedIndex = 0;

    private void UpdateSelectedItem()
    {
        var count = 0;
        foreach (Transform cassette in _cassetteListRoot)
        {
            if (count == _selectedIndex)
            {
                cassette.SendMessage("OnMouseEnter");
                break;
            }
            count++;
            cassette.SendMessage("OnMouseExit");
        }
    }

    private void UpdateAnimation()
    {
        switch (State)
        {
            case AnimationState.Closed:
            case AnimationState.Open:
                break;
            case AnimationState.Closeing:
                _inventoryTransform.anchoredPosition
                    += Vector2.down * _animationDelta * Time.deltaTime;
                if (_inventoryTransform.anchoredPosition.y <= _closedPosition)
                {
                    _inventoryTransform.anchoredPosition
                        = new Vector2(_inventoryTransform.anchoredPosition.x, _closedPosition);
                    State = AnimationState.Closed;
                }
                break;
            case AnimationState.Opening:
                _inventoryTransform.anchoredPosition
                    += Vector2.up * _animationDelta * Time.deltaTime;
                if (_inventoryTransform.anchoredPosition.y >= _openPosition)
                {
                    _inventoryTransform.anchoredPosition
                        = new Vector2(_inventoryTransform.anchoredPosition.x, _openPosition);
                    State = AnimationState.Open;
                }
                break;
        }
    }

    public void Add(Tape tape)
    {
        _tapes.Add(tape);
        UpdateItems();
    }

    private void UpdateItems()
    {
        foreach (Transform child in _cassetteListRoot)
        {
            Destroy(child);
        }
        foreach (var tape in _tapes)
        {
            Instantiate(_cassetteObject, _cassetteListRoot);
        }
    }

    public void ToggleInventory()
    {
        switch (State)
        {
            case AnimationState.Closeing:
            case AnimationState.Closed:
                State = AnimationState.Opening;
                break;
            case AnimationState.Opening:
            case AnimationState.Open:
                State = AnimationState.Closeing;
                break;
        }
    }
}
