﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class SpecialAbilityBar : MonoBehaviour
{
	// Update is called once per frame
	public void UpdateBar (float percentage)
    {
        this.GetComponent<Image>().fillAmount = percentage;
    }
}
