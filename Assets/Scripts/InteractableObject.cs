﻿using System.Collections;
using UnityEngine;

public class InteractableObject : MonoBehaviour
{
    [SerializeField] private float _maxBounciness = 0.3f;
    [SerializeField] private float _minBounciness = 0.1f;
    [SerializeField] private float _frequency = 20; // bounce pr second
    [SerializeField] private float _bounceDuration = 5; // bounce pr second
    [SerializeField] private ParticleSystem _particleSystem;
    [SerializeField] private AudioClip _audioClip;

    private bool _cancelBounce = false;

    private float _bounciness;
    private Vector3 _scale;

    public void Awake()
    {
        _bounciness = 0;
        _scale = transform.localScale;
    }

    public void Update()
    {
        var sin = Mathf.Sin(Time.time * _frequency * Mathf.PI * 2) * _bounciness;
        transform.localScale = _scale + new Vector3(sin, -sin);
        _bounciness -= Time.deltaTime * .1f;
        if (_bounciness > _maxBounciness)
            OnBounceFinished();

        _bounciness = Mathf.Clamp(_bounciness, 0, _maxBounciness);
    }

    public void OnSineHit()
    {
        _bounciness += _minBounciness;
    }

    protected void OnBounceFinished()
    {
        _bounciness = 0;
        _particleSystem.Emit(Random.Range(10, 100));
        Destroy(gameObject, 10);
        AudioSource.PlayClipAtPoint(_audioClip, transform.position);
        GetComponent<BoxCollider2D>().enabled = false;
        foreach (Transform child in transform)
        {
            child.gameObject.SetActive(false);
        }
    }
}
