﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OverlayMusicHandler : MonoBehaviour
{
    public float _fadeDuration = 2f;
    public float _maxVolume = 1f;
    public AudioSource _audioSource;

    private float _startTime = 0;

    void Update()
    {
        _audioSource.volume = Mathf.Max(0, Mathf.Lerp(_maxVolume, 0, (Time.time - _startTime) / _fadeDuration));
    }

    public void Ping()
    {
        _startTime = Time.time;
    }
}
