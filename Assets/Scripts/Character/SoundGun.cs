﻿using System.Linq;
using UnityEngine;

public class SoundGun : MonoBehaviour
{
    public const string MESSAGE_SHOOT = "Shoot";
    [SerializeField] private float _radius = 10f;
    [SerializeField] private float _angle = 35f;

    public void Shoot()
    {
        // TODO: Play Shoot Effects (Animation + Sound)

        var objects = Physics2D.OverlapCircleAll(transform.position, _radius)
            .Where(o =>
            {
                var dir = (o.transform.position - transform.position).normalized;
                var dot = Mathf.Acos(Vector3.Dot(transform.forward, dir));
                return Mathf.Acos(dot) <= _angle/2;
            });

        foreach (var o in objects)
        {
            o.SendMessage(MESSAGE_SHOOT);
        }
    }

    public void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawRay(transform.position, Quaternion.Euler(0 ,0, _angle/2)*transform.right*_radius); 
        Gizmos.DrawRay(transform.position, Quaternion.Euler(0, 0, -_angle/2) *transform.right*_radius); 
    }
}
