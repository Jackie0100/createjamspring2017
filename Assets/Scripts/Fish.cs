﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fish : Enemy
{
    [SerializeField]
    float timeoffset;
    [SerializeField]
    float heigth;
    [SerializeField]
    float speed;

    // Update is called once per frame
    void Update ()
    {
        transform.position = new Vector3(transform.position.x, -2 + Mathf.Sin((Time.time - timeoffset) * speed * Mathf.Rad2Deg) * heigth, transform.position.z);
    }
}
