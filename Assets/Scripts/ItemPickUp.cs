﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemPickUp : MonoBehaviour
{
    protected virtual void Start ()
    {
		
	}
	
	protected virtual void Update () {
		
	}

    protected virtual void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            PickUp(collision.GetComponent<Player>());
        }
    }

    protected virtual void PickUp(Player player)
    {
        Destroy(this.gameObject);
    }
}
