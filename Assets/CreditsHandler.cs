﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreditsHandler : MonoBehaviour
{
    [SerializeField] private GameObject _mainMenu;

    public void Back()
    {
        _mainMenu.SetActive(true);
        gameObject.SetActive(false);
    }
}
