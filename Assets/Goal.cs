﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Goal : MonoBehaviour
{
    [SerializeField] private int _nextScene;
    [SerializeField] private AudioClip _audioClip;
    [SerializeField] private Vector2 _nextStartPosition;
    [SerializeField] private bool _shouldDestroyPlayer = false;

    private bool isTriggered = false;
    private Transform _playerTransform;

    public void OnTriggerEnter2D(Collider2D collider)
    {
        if (isTriggered) return;

        if (collider.tag == "Player")
        {
            isTriggered = true;
            _playerTransform = collider.transform;
            StartCoroutine(FinishLevel());
        }
    }

    private IEnumerator FinishLevel()
    {
        Player.instance.musicSource.Pause();
        AudioSource.PlayClipAtPoint(_audioClip, transform.position);
        yield return new WaitForSeconds(5);
        _playerTransform.position = _nextStartPosition;
        Player.instance.musicSource.UnPause();
        SceneManager.LoadScene(_nextScene);
        if (_shouldDestroyPlayer)
        {
            Destroy(_playerTransform.gameObject);
        }
    }
}
