﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuHandler : MonoBehaviour
{
    [SerializeField] private GameObject _credits;
    [SerializeField] private GameObject _playlist;

    public void StartGame()
    {
        SceneManager.LoadScene(1);
    }

    public void PlayList()
    {
        _playlist.SetActive(true);
        gameObject.SetActive(false);
    }

    public void Credits()
    {
        _credits.SetActive(true);
        gameObject.SetActive(false);
    }

    public void Exit()
    {
        Application.Quit();
    }
}
