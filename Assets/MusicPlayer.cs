﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicPlayer : MonoBehaviour
{
    [SerializeField] private AudioClip[] _audioClips;
    [SerializeField] private AudioSource _audioSource;
    [SerializeField] private GameObject _mainMenu;

    public void Play(int i)
    {
        _audioSource.clip = _audioClips[i];
        _audioSource.Play();
    }

    public void Back()
    {
        _mainMenu.SetActive(true);
        gameObject.SetActive(false);
    }
}
