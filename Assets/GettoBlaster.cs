﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GettoBlaster : MonoBehaviour {
    void FixedUpdate()
    {
        var pColliders = Physics2D.OverlapCircleAll(transform.position, 3, LayerMask.GetMask("Player"));
        foreach (var col in pColliders)
        {
            if(col.GetComponent<Rigidbody2D>()!=null)
                col.GetComponent<Rigidbody2D>().AddForce((col.transform.position - transform.position).normalized * 50f, ForceMode2D.Force);
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        GetComponent<ParticleSystem>().Stop();
        GetComponent<AudioSource>().Stop();
        GetComponent<CircleCollider2D>().enabled = false;
        enabled = false;
    }
}
